<!doctype html>

<html>

<head>
<title> Upload - ModernTube </title>
<link rel="stylesheet" type="text/css" href="moderntube.css">
</head>

<body>

<a href="index.php"> Home </a>

<form action="upload.php" method="post" enctype="multipart/form-data">
	<label>Name: </label> <input type="text" name="name" required="true"> <br>
    <label>Select file to upload: </label> <input type="file" name="fileToUpload" id="fileToUpload"> <br>
	<label> Media type: </label>
	<select name="type" required="true">
		<option value=0>Image</option>
		<option value=1>Audio</option>
		<option value=2>Video</option>
	</select><br>
	<label>Description: </label> <br> <textarea id="media_description" name="media_description" rows="4" cols="50"> </textarea> <br>
	<label>Tags (separate with newline): </label> <br> <textarea id="tag_list" name="tag_list" rows="4" cols="50"> </textarea> <br>
    <input type="submit" value="Upload File" name="submit"> <input name="reset" type="reset">
</form>

<?php
	include_once "database.php";
	session_start();

	if(isset($_POST["submit"])){ do{

		if($_FILES["fileToUpload"]["error"]){
			echo "There was an error with your upload: ";
			switch($_FILES["fileToUpload"]["error"]){
				case 1:
					echo "The uploaded file exceeds the upload_max_filesize directive in php.ini.";
					break;
				case 2:
					echo "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.";
					break;
				case 3:
					echo "The uploaded file was only partially uploaded.";
					break;
				case 4:
					echo "No file was uploaded.";
					break;
				case 6:
					echo "Missing a temporary folder. Introduced in PHP 5.0.3.";
					break;
				case 7:
					echo "Failed to write file to disk. Introduced in PHP 5.1.0.";
					break;
				case 8:
					echo "A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help. Introduced in PHP 5.2.0.";
					break;
			}
			break;
		}

		$target_file = "uploads/" . basename($_FILES["fileToUpload"]["name"]);
		$file_extention = 0;

		while(file_exists($target_file)){
			$target_file = "uploads/" . $file_extention . basename($_FILES["fileToUpload"]["name"]);
			$file_extention++;
		}

		if(!move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)){
			echo "Sorry, there was an error uploading " . $_FILES["fileToUpload"]["tmp_name"];
			break;
		}

		chmod($target_file, 0644);

		$tags = htmlspecialchars($_POST['tag_list']);
		$keywords = htmlspecialchars($_POST['name']);
		$tagarray = explode(PHP_EOL, $tags);
		$keywordarray = explode(" ",$keywords);

		$handle = realpath($_FILES["fileToUpload"]["tmp_name"]);
		$temp_uploader_id = $database->query("select user_id from Users where username=\"$_SESSION[login_user]\"")->fetch_assoc()["user_id"];
		$ip = (int) $_SERVER['REMOTE_ADDR'];

		// Insert the media into the table
		$database->query("INSERT INTO Media (uploader_id, upload_time, name, description, type, filepath, views, uploader_ip) 
			VALUES ($temp_uploader_id , now(), \"$_POST[name]\", \"$_POST[media_description]\", $_POST[type], \"$target_file\", 0, $ip)");
		echo "$database->error";

		// Insert the tags
		$mymedia = $database->query("select media_id from Media ORDER BY media_id DESC LIMIT 1")->fetch_assoc()["media_id"];

		foreach($tagarray as $i => $item){
			$database->query("INSERT INTO Media_Tags (media_id, tag) VALUES ($mymedia , \"$tagarray[$i]\")");
			echo "$database->error";
		}

		// Insert the keywords
		foreach($keywordarray as $i => $item){
			$database->query("INSERT INTO Media_Keywords (media_id, keyword) VALUES ($mymedia , \"$keywordarray[$i]\")");
			echo "$database->error";
		}

		// File uploaded successfully
		echo "The file " . basename($_FILES["fileToUpload"]["name"]) . " has been uploaded.";

	} while(false); }

	$database->close();
?>

</body>

</html>



