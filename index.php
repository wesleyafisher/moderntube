<!doctype html>

<html>

<head>
<title> ModernTube </title>
<link rel="stylesheet" type="text/css" href="moderntube.css">
</head>

<body>

<h1> Welcome to ModernTube! </h1>

<?php
	include_once "database.php";
	session_start();

	if(isset($_SESSION["login_user"]) && $_SESSION["login_user"] != ""){
		echo "<a href=\"logout.php\">Logout</a> <a href=\"upload.php\">Upload</a> <a href=\"profile.php\">Profile</a>";
	}

	else{
		echo "<a href=\"login.php\">Login</a>";
	}
?>

<form action="index.php" method="get">
	<label>Search ModernTube </label>
	<input type="search" name="search">
	<select name="category" required="true">
		<option value=3>All</option>
		<option value=0>Image</option>
		<option value=1>Audio</option>
		<option value=2>Video</option>
	</select>
	<input type="submit" name="submit" value="Search">
</form>


<?php
	if(isset($_GET["submit"])){ do{

		switch($_GET["category"]){
			case 0:
				if($_GET["search"] == "") echo "<h2>All Images</h2>";
				else echo "<h2>Image Search Results for \"$_GET[search]\"</h2>";
				break;

			case 1:
				if($_GET["search"] == "") echo "<h2>All Audio</h2>";
				else echo "<h2>Audio Search Results for \"$_GET[search]\"</h2>";
				break;

			case 2:
				if($_GET["search"] == "") echo "<h2>All Videos</h2>";
				else echo "<h2>Video Search Results for \"$_GET[search]\"</h2>";
				break;

			case 3:
				if($_GET["search"] == "") echo "<h2>All Media</h2>";
				else echo "<h2>All Search Results for \"$_GET[search]\"</h2>";
				break;
		}

		// Search based on tags and keywords the user submitted
		$search_results = $database->query("select Media.media_id, Media.name, Media.views, Media.type from Media, Media_Keywords 
			where Media.media_id=Media_Keywords.media_id and \"$_GET[search]\" like concat(\"%\",Media_Keywords.keyword,\"%\") 
			union 
			select Media.media_id, Media.name, Media.views, Media.type from Media, Media_Keywords where Media.media_id=Media_Keywords.media_id 
			and Media_Keywords.keyword like \"%$_GET[search]%\" 
			union 
			select Media.media_id, Media.name, Media.views, Media.type from Media, Media_Tags 
			where Media.media_id=Media_Tags.media_id and \"$_GET[search]\" like concat(\"%\",Media_Tags.tag,\"%\") 
			union 
			select Media.media_id, Media.name, Media.views, Media.type from Media, Media_Tags 
			where Media.media_id=Media_Tags.media_id and Media_Tags.tag like \"%$_GET[search]%\"");
		echo "$database->error";

		while($row = $search_results->fetch_assoc()){

			// Check if the type of media is the type the user wants
			if($_GET["category"] != $row["type"] && $_GET["category"] != 3) continue 1;

			// Display media info
			$rating = $database->query("select avg(rating) as rate from Media_Ratings where media_id=$row[media_id]")->fetch_assoc()["rate"];
			echo "$database->error";
			if(!$rating) $rating = "Not rated";
			echo "<a href=\"watch.php?media_id=$row[media_id]\">$row[name]</a><br>";
			echo "Views: $row[views]\t Rating: $rating <br><br>";
		}

	} while(false); }

	else{ do{

		echo "<h2>Most Popular Media</h2>";

		// Get 10 most popular media
		$pop_media = $database->query("select media_id, name, views from Media order by views desc limit 10");
		echo "$database->error";

		while($row = $pop_media->fetch_assoc()){

			// Display media info
			$rating = $database->query("select avg(rating) as rate from Media_Ratings where media_id=$row[media_id]")->fetch_assoc()["rate"];
			echo "$database->error";
			if(!$rating) $rating = "Not rated";
			echo "<a href=\"watch.php?media_id=$row[media_id]\">$row[name]</a><br>";
			echo "Views: $row[views]\t Rating: $rating <br><br>";
		}

		echo "<h2>Newest Media</h2>";

		// Get 10 newest media
		$new_media = $database->query("select media_id, name, views from Media order by upload_time desc limit 10");
		echo "$database->error";

		while($row = $new_media->fetch_assoc()){

			// Display media info
			$rating = $database->query("select avg(rating) as rate from Media_Ratings where media_id=$row[media_id]")->fetch_assoc()["rate"];
			echo "$database->error";
			if(!$rating) $rating = "Not rated";
			echo "<a href=\"watch.php?media_id=$row[media_id]\">$row[name]</a><br>";
			echo "Views: $row[views]\t Rating: $rating <br><br>";
		}

	} while(false); }
?>

<?php
	$database->close();
?>

</body>

</html>
