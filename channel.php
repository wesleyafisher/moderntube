<!doctype html>
<html>

<head>
<title> Channel - ModernTube </title>
<link rel="stylesheet" type="text/css" href="moderntube.css">
</head>

<body>



<?php
	include_once "database.php";
	session_start();

	if(isset($_SESSION["login_user"]) && $_SESSION["login_user"] != ""){
		echo "<a href=\"index.php\"> Home </a> <a href=\"profile.php\"> Profile </a>";
	}
	else{
		echo "<a href=\"index.php\"> Home </a>";			
	}

	if(isset($_GET["user_id"])){ do{

		$channel_user = $database->query("select * from Users where user_id=\"$_GET[user_id]\"")->fetch_assoc();
		echo "$database->error";

		// Introduction
		echo "<h1>Welcome to $channel_user[display_name]'s channel!</h1>";

		// Channel Stats
		echo "<p> $channel_user[display_name]'s Stats </p>";

		echo "<label>Join date: $channel_user[join_date]</label><br>";
	
		$num_uploads = $database->query("select count(media_id) as num from Media where uploader_id=\"$channel_user[user_id]\"")->fetch_assoc()["num"];
		echo "$database->error";
		echo "<label>Number of uploads: $num_uploads</label><br>";

		$total_views = $database->query("select sum(views) as num from Media where uploader_id=\"$channel_user[user_id]\"")->fetch_assoc()["num"];
		echo "$database->error";
		if(!$total_views) $total_views = 0;
		echo "<label>Total views: $total_views</label><br>";

		// Playlists
		echo "<p> $channel_user[display_name]'s Playlists </p>";
		$playlists = $database->query("select playlist_id, name from Playlists where playlist_creator_id=\"$channel_user[user_id]\"");
		echo "$database->error";
		while($row = $playlists->fetch_assoc()){
			echo "<a href=\"playlist.php?playlist_id=$row[playlist_id]\">$row[name]</a><br>";
		}

		// Uploads
		echo "<p> $channel_user[display_name]'s Uploads </p>";

		$my_media = $database->query("select media_id, name, views from Media where uploader_id=\"$channel_user[user_id]\" order by views desc");
		echo "$database->error";
		while($row = $my_media->fetch_assoc()){
			$rating = $database->query("select avg(rating) as rate from Media_Ratings where media_id=$row[media_id]")->fetch_assoc()["rate"];
			echo "$database->error";
			if(!$rating) $rating = "Not rated";
			echo "<a href=\"watch.php?media_id=$row[media_id]\">$row[name]</a><br>";
			echo "Views: $row[views]\t Rating: $rating <br><br>";
		}

	} while(false); }

	else{ do{

	    echo "<h1>Something went very wrong! Get out while you still can!</h1>";

	} while(false); }

	$database->close();

?>

</body>

</html>
