<!doctype html>
<html>

<head>
<title> Watch - ModernTube </title>
<link rel="stylesheet" type="text/css" href="moderntube.css">
</head>

<body>

<a href="index.php"> Home </a>

<?php
	include_once "database.php";
	session_start();

	if(isset($_GET["media_id"])){ do{

		if(isset($_SESSION["login_user"]) && $_SESSION["login_user"] != ""){
			$login_user = $database->query("select * from Users where username=\"$_SESSION[login_user]\"")->fetch_assoc();
			echo "$database->error";
		}

		// Check if the logged in user is blocked from this media, if so display a message and return;
		if(isset($_SESSION["login_user"]) && $_SESSION["login_user"] != ""){
			$blocked_user = $database->query("select Users.display_name from Users, Media_Blocklist where Users.user_id=Media_Blocklist.blocked_id and media_id=$_GET[media_id] and blocked_id=$login_user[user_id]")->fetch_assoc();
			echo "$database->error";
			if($blocked_user){
				echo "<h1>YOU DO NOT BELONG HERE $blocked_user[display_name]</h1>";
				$database->close();
				return;
			}
		}

		// Get the information for the media on the current page
		$media = $database->query("select * from Media where media_id=$_GET[media_id]")->fetch_assoc();
		echo "$database->error";

		// Get the information for the uploader of the media
		$uploader = $database->query("select * from Users where user_id=$media[uploader_id]")->fetch_assoc();
		echo "$database->error";

		// Increment the number of views of the media
		$new_views = $media["views"] + 1;
		$database->query("update Media set views=$new_views where media_id=$media[media_id]");
		echo "$database->error";

		// Print the Media name
		echo "<h1>$media[name]</h1>";

		// Display the Media using the correct HTML container based on the media type
		switch($media["type"]){
			case 0:
				echo "<img src=\"$media[filepath]\" alt=\"Couldn't display image\"><br>";
				break;
			case 1:
				echo "<audio controls>
					<source src=\"$media[filepath]\">
  					Your browser does not support the audio tag.
				</audio><br>";
				break;
			case 2:
				echo "<video controls>
					<source src=\"$media[filepath]\">
  					Your browser does not support the video tag.
				</video><br>";
				break;
		}

		// Display the user display_name that uploaded the video (make this a link to channel later)
		echo "<a href=\"channel.php?user_id=$uploader[user_id]\">$uploader[display_name]</a><br>";

		// Display the number of views for the media
		echo "$media[views] views<br>";

		// Display the media rating
		$rating = $database->query("select avg(rating) as rate from Media_Ratings where media_id=$media[media_id]")->fetch_assoc()["rate"];
		if(!$rating) $rating = "Not rated";
		echo "Rating: $rating <br>";

		// Allow users to rate media
		if(isset($_SESSION["login_user"]) && $_SESSION["login_user"] != ""){
			echo "<form action=\"watch.php?media_id=$_GET[media_id]\" method=\"post\">
				<label>Rate this media: </label>
				<select name=\"Rate\" required=\"true\">
					<option value=1>1</option>
					<option value=2>2</option>
					<option value=3>3</option>
					<option value=4>4</option>
					<option value=5>5</option>
				</select>
				<input type=\"submit\" name=\"submit_rating\" value=\"Submit Rating\">
			</form>";
		}

		if(isset($_POST["submit_rating"])){ do {
			//check if user hasn't already submitted
			$check = $database->query("select rater_id from Media_Ratings where rater_id=\"$login_user[user_id]\" and media_id = $media[media_id]")->fetch_assoc();
			echo "$database->error";
			if($check){
				echo "You have already rated this content!";
				break;
			}
			$database->query("INSERT INTO Media_Ratings (rater_id, media_id, rating) VALUES ($login_user[user_id], $media[media_id], $_POST[Rate] )");
			echo "$database->error";
			echo "Rating received";
		} while (false);}


		// Upload date of media
		echo "Upload date: $media[upload_time]<br>";

		// Description of media
		echo "<b>Description</b><br>";
		echo "$media[description]<br>";

		// Tags of media
		$tags = $database->query("select * from Media_Tags where media_id=$media[media_id]");
		echo "<b>Tags</b><br>";
		if(!$tags) {
			echo "No tags<br>";
		}
		else {
			while($row = $tags->fetch_assoc()){
				echo "$row[tag]\t";
			}
			echo "<br>";
		}

		// Form for user to block people from media (uploader only)


	 	// Form for user to add this media to their playlist
		if(isset($_SESSION["login_user"]) && $_SESSION["login_user"] != ""){

			$playlists = $database->query("select name, playlist_id from Playlists where playlist_creator_id = $login_user[user_id]");
			echo "$database->error";

			echo "<form action =\"watch.php?media_id=$media[media_id]\" method=\"post\">";
				echo "<select name = \"playlist\" required =\"true\">";
					while ($row = $playlists->fetch_assoc()) {
						echo "<option value=$row[playlist_id]>$row[name]</option>";
					}
				echo "</select>";
				echo "<input type=\"submit\" name=\"playlist_add\" value=\"Add to playlist\">";
			echo "</form>";
		}


	 	if(isset($_POST["playlist_add"])){ do {
			//check if user hasn't already submitted
			$check = $database->query("select add_date from Playlist_Media where playlist_id=$_POST[playlist] and media_id=$media[media_id]")->fetch_assoc();
			echo "$database->error";
			if($check){
				echo "You have already added this to the playlist!";
				break;
			}
			$database->query("INSERT INTO Playlist_Media (playlist_id, media_id, add_date)
			VALUES ($_POST[playlist], $media[media_id], now() )");
			echo "$database->error";
			echo "Added to playlist!";
		} while (false);}


		// Download button
		if(isset($_SESSION["login_user"]) && $_SESSION["login_user"] != ""){
			$myfilepath = $database->query("select filepath from Media where media_id=$media[media_id]")->fetch_assoc()["filepath"];
			echo "$database->error";
			echo "<a href=\"$myfilepath\" download>Download</a><br>";
		}

		// Comments (display existing) display the display_name of the user that commented, when they commented, and the comment
		$comments = $database->query("select * from Media_Comments where media_id=$media[media_id]");

		echo "$database->error";
		echo "<b>Comments</b><br>";
		if(!$comments) {
			echo "No comments<br>";
		}
		else {
			while($row = $comments->fetch_assoc()){
				$commenter_name = $database->query("select display_name from Users where user_id=$row[commenter_id]")->fetch_assoc()["display_name"];
				echo "$commenter_name\t $row[post_date]\t <br>";
				echo "$row[comment]<br>";
			}
		}

		// Comments form to add new comment
		if(isset($_SESSION["login_user"]) && $_SESSION["login_user"] != ""){
			echo "<form action=\"watch.php?media_id=$_GET[media_id]\" method=\"post\">
				<label>Add Comment: </label> <br> <textarea id=\"comment_description\" name=\"comment_description\" rows=\"4\" cols=\"50\"> </textarea> <br>
			    <input type=\"submit\" value=\"Submit Comment\" name=\"commentValue\">
			</form>";
		}

		if(isset($_POST["commentValue"])){ do {
			$database->query("INSERT INTO Media_Comments (commenter_id, media_id, post_date, comment)
			VALUES ($login_user[user_id], $media[media_id], now(), \"$_POST[comment_description]\" )");
			echo "$database->error";
			echo "Comment Posted";
		} while (false);}

		// Discussion group

		// Links to recommended videos on the side

	} while(false); }

	else{ do{

	    echo "<h1>Something went very wrong! Get out while you still can!</h1>";

	} while(false); }

	$database->close();

?>

</body>

</html>
