<!doctype html>

<html>

<head>
<title> Register - ModernTube </title>
<link rel="stylesheet" type="text/css" href="moderntube.css">
</head>

<body>

<a href="index.php"> Home </a>

<form action="register.php" method="post">
	<label>Username: </label> <input type="text" name="username" required="true"> <br>
	<label>Email: </label> <input type="email" name="email" required="true"> <br>
	<label>Display Name: </label> <input type="text" name="display_name" required="true"> <br>
	<label>Password: </label> <input type="password" name="password1" required="true"> <br>
	<label>Confirm Password: </label> <input type="password" name="password2" required="true"> <br>
	<input name="submit" type="submit" value="Register">
	<input name="reset" type="reset">
</form>

<?php
	include_once "database.php";
	session_start();

	if(isset($_POST["submit"])){ do{

		// Check if passwords match
		if($_POST["password1"] != $_POST["password2"]) {
			echo "Your passwords don't match, try again.";
			return;
		}

		// Check if the user already exists
		$userexists = $database->query("select username from Users where username=\"$_POST[username]\"")->num_rows;
		echo "$database->error";
		if($userexists){
			echo "User \"$_POST[username]\" already exists. Please use a different username.";
			return;
		}

		// Add user
		$database->query("insert into Users (username, password, join_date, email, display_name) values (\"$_POST[username]\", \"$_POST[password1]\", now(), \"$_POST[email]\", \"$_POST[display_name]\")");
		echo "$database->error";

		// Add Favorites playlist to the newly created user
		$user = $database->query("select user_id from Users where username=\"$_POST[username]\"")->fetch_assoc();
		echo "$database->error";
		$database->query("insert into Playlists (playlist_creator_id, name, creation_date) values (\"$user[user_id]\", \"Favorites\", now())");
		echo "$database->error";

		echo "Registration successful. Welcome to ModernTube, $_POST[display_name].";

	} while(false); }

	$database->close();
?>

</body>

</html>
