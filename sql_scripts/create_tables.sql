create table Users(
	user_id bigint unsigned not null auto_increment primary key,
	username varchar(32) unicode not null,
	password varchar(32) unicode not null,
	join_date datetime not null,
	email varchar(64) unicode not null,
	display_name varchar(64) unicode not null
);

create table Media(
	media_id bigint unsigned not null auto_increment primary key,
	uploader_id bigint unsigned not null references Users(user_id),
	foreign key(uploader_id) references Users(user_id),
	upload_time datetime not null,
	name varchar(64) unicode not null,
	description varchar(2048) unicode not null,
	type tinyint unsigned not null,
	filepath varchar(64) unicode not null,
	views bigint unsigned not null,
	uploader_ip int unsigned not null
);

create table Media_Blocklist(
	blocked_id bigint unsigned not null references Users(user_id),
	foreign key(blocked_id) references Users(user_id),
	media_id bigint unsigned not null references Media(Media_id),
	foreign key(media_id) references Media(media_id),
	primary key(blocked_id, media_id),
	block_date datetime not null
);

create table Playlists(
	playlist_id bigint unsigned not null auto_increment primary key,
	playlist_creator_id bigint unsigned not null references Users(user_id),
	foreign key(playlist_creator_id) references Users(user_id),
	name varchar(64) unicode not null,
	creation_date datetime not null
);

create table Playlist_Media(
	playlist_id bigint unsigned not null references Playlists(playlist_id),
	foreign key(playlist_id) references Playlists(playlist_id),
	media_id bigint unsigned not null references Media(media_id),
	foreign key(media_id) references Media(media_id),
	primary key(playlist_id, media_id),
	add_date datetime not null
);

create table User_Contacts(
	contacter_id bigint unsigned not null references Users(user_id),
	foreign key(contacter_id) references Users(user_id),
	contact_id bigint unsigned not null references Users(user_id),
	foreign key(contact_id) references Users(user_id),
	primary key(contacter_id, contact_id),
	add_date datetime not null
);

create table Media_Comments(
	comment_id bigint unsigned not null auto_increment primary key,
	commenter_id bigint unsigned not null references Users(user_id),
	foreign key(commenter_id) references Users(user_id),
	media_id bigint unsigned not null references Media(media_id),
	foreign key(media_id) references Media(media_id),
	post_date datetime not null,
	comment varchar(512) unicode not null
);

create table Media_Ratings(
	rater_id bigint unsigned not null references Users(user_id),
	foreign key(rater_id) references Users(user_id),
	media_id bigint unsigned not null references Media(media_id),
	foreign key(media_id) references Media(media_id),
	primary key(rater_id, media_id),
	rating tinyint not null
);

create table User_Blocklist(
	blocker_id bigint unsigned not null references Users(user_id),
	foreign key(blocker_id) references Users(user_id),
	blocked_id bigint unsigned not null references Users(user_id),
	foreign key(blocked_id) references Users(user_id),
	primary key(blocker_id, blocked_id),
	block_date datetime not null
);

create table Downloads(
	download_id bigint unsigned not null auto_increment primary key,
	downloader_id bigint unsigned not null references Users(user_id),
	foreign key(downloader_id) references Users(user_id),
	media_id bigint unsigned not null references Media(media_id),
	foreign key(media_id) references Media(media_id),
	download_time datetime not null,
	downloader_ip int unsigned not null
);

create table Media_Keywords(
	media_id bigint unsigned not null references Media(media_id),
	foreign key(media_id) references Media(media_id),
	keyword varchar(32) unicode not null,
	primary key(media_id, keyword)
);

create table Media_Tags(
	media_id bigint unsigned not null references Media(media_id),
	foreign key(media_id) references Media(media_id),
	tag varchar(64) unicode not null,
	primary key(media_id, tag)
);

create table Media_Discussion_Group(
	discussion_id bigint unsigned not null auto_increment primary key,
	creator_id bigint unsigned not null references Users(user_id),
	foreign key(creator_id) references Users(user_id),
	media_id bigint unsigned not null references Media(media_id),
	foreign key(media_id) references Media(media_id),
	name varchar(64) unicode not null,
	creation_date datetime not null
);

create table Media_Discussion_Comments(
	comment_id bigint unsigned not null auto_increment primary key,
	commenter_id bigint unsigned not null references Users(user_id),
	foreign key(commenter_id) references Users(user_id),
	discussion_id bigint unsigned not null references Media_Discussion_Group(discussion_id),
	foreign key(discussion_id) references Media_Discussion_Group(discussion_id),
	post_date datetime not null,
	comment varchar(512) unicode not null
);

create table Messages(
	message_id bigint unsigned not null auto_increment primary key,
	sender_id bigint unsigned not null references Users(user_id),
	foreign key(sender_id) references Users(user_id),
	receiver_id bigint unsigned not null references Users(user_id),
	foreign key(receiver_id) references Users(user_id),
	title varchar(128) unicode not null,
	message varchar(2048) unicode not null,
	send_date datetime not null
);
