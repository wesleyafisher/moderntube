<!doctype html>
<html>

<head>
<title> Message Center - ModernTube </title>
<link rel="stylesheet" type="text/css" href="moderntube.css">
</head>

<body>

<a href="index.php"> Home </a>

<?php
	include_once "database.php";
	session_start();

	$user = $database->query("select * from Users where username=\"$_SESSION[login_user]\"")->fetch_assoc();
	echo "$database->error";

	echo "<h1>Welcome to your message center $user[display_name]</h1>";

	echo "<h3>Manage contacts</h3>";
?>

<p> Blocked Users </p>

<?php

	$contacts = $database->query("select Users.display_name from Users, User_Blocklist where blocker_id=\"$user[user_id]\" and blocked_id=user_id");
	echo "$database->error";
	while($row = $contacts->fetch_assoc()){
		echo "$row[display_name]<br>";
	}
	echo "<br>";

?>

<form action="messagecenter.php" method="post">
	<label>Block User</label><br>
	<label>Blocked User Username:</label><input type="text" name="blocked_user_username" required="true"><br>
	<input type="submit" name="block_user_submit" value="Block User"><br>
</form>

<?php

	if(isset($_POST["block_user_submit"])){ do{

		// Check if the user you are trying to block exists
		$blocked_user = $database->query("select user_id from Users where username=\"$_POST[blocked_user_username]\"")->fetch_assoc();
		echo "$database->error";
		if(!$blocked_user){
			echo "The user you are trying to block, \"$_POST[blocked_user_username]\" does not exist";
			break;
		}

		//Check if the user you are trying to block is yourself
		if($blocked_user["user_id"] == $user["user_id"]){
			echo "Don't hate yourself so much that you try to block youself. Take some pride in yourself.";
			break;
		}

		// Check if you have already blocked that user
		$check = $database->query("select block_date from User_Blocklist where blocker_id=\"$user[user_id]\" and blocked_id=\"$blocked_user[user_id]\"")->fetch_assoc();
		echo "$database->error";
		if($check){
			echo "You already blocked $_POST[blocked_user_username] on $check[block_date]";
			break;
		}

		// Remove yourself from the blocked user's contact list
		$database->query("delete from User_Contacts where contacter_id=\"$blocked_user[user_id]\" and contact_id=\"$user[user_id]\"");
		echo "$database->error";

		// Add blocked user
		$database->query("insert into User_Blocklist (blocker_id, blocked_id, block_date) values (\"$user[user_id]\", \"$blocked_user[user_id]\", now())");
		echo "$database->error";
		echo "User blocked";

	} while(false); }

?><br>

<p> Contacts </p>

<?php

	$contacts = $database->query("select Users.display_name from User_Contacts, Users where contacter_id=\"$user[user_id]\" and contact_id=user_id");
	echo "$database->error";
	while($row = $contacts->fetch_assoc()){
		echo "$row[display_name]<br>";
	}
	echo "<br>";

?>

<form action="messagecenter.php" method="post">
	<label>Add Contact</label><br>
	<label>Contact Username:</label><input type="text" name="contact_username" required="true"><br>
	<input type="submit" name="add_contact_submit" value="Add Contact"><br>
</form>

<?php

	if(isset($_POST["add_contact_submit"])){ do{

		// Check if the user you are trying to add exists
		$contact = $database->query("select user_id from Users where username=\"$_POST[contact_username]\"")->fetch_assoc();
		echo "$database->error";
		if(!$contact){
			echo "The user you are trying to add, \"$_POST[contact_username]\" does not exist";
			break;
		}

		//Check if the user you are trying to add is yourself
		if($contact["user_id"] == $user["user_id"]){
			echo "Trying to add yourself as a contact so you will technically have friends, yikes.";
			break;
		}

		// Check if the user has already added the contact
		$check = $database->query("select add_date from User_Contacts where contacter_id=\"$user[user_id]\" and contact_id=\"$contact[user_id]\"")->fetch_assoc();
		echo "$database->error";
		if($check){
			echo "You already added $_POST[contact_username] on $check[add_date]";
			break;
		}

		// Check if contact has the user blocked
		$check = $database->query("select block_date from User_Blocklist where blocked_id=\"$user[user_id]\" and blocker_id=\"$contact[user_id]\"")->fetch_assoc();
		echo "$database->error";
		if($check){
			echo "$_POST[contact_username] blocked you on $check[block_date]";
			break;
		}

		// Add contact
		$database->query("insert into User_Contacts (contacter_id, contact_id, add_date) values (\"$user[user_id]\", \"$contact[user_id]\", now())");
		echo "$database->error";
		echo "Contact added";

	} while(false); }

?><br>

<h3> My messages </h3>

<?php
	$messages = $database->query("select * from Messages where receiver_id=$user[user_id]");
	echo "$database->error";

	while($row = $messages->fetch_assoc()){
		$sender_name = $database->query("select display_name from Users where user_id=$row[sender_id]")->fetch_assoc()["display_name"];
		echo "<a href=\"message.php?message_id=$row[message_id]\" target=\"_blank\">$row[title]</a>";
		echo " from $sender_name on $row[send_date]<br>";
	}
?>

<h3> Send a message </h3>

<?php
	echo "<form action=\"messagecenter.php\" method=\"post\">";
	echo "<label>Recipient(s)</label><br>";

	$contacts = $database->query("select Users.display_name, Users.user_id from User_Contacts, Users where contacter_id=$user[user_id] and contact_id=user_id");
	echo "$database->error";
	$num_contacts = $contacts->num_rows;

	for($i = 0; $i < $num_contacts; $i++){
		$contact = $contacts->fetch_assoc();
		echo "<input type=checkbox name=contact$i value=$contact[user_id]>$contact[display_name]";
	}

	echo "<br>";
	echo "<label>Title</label><br> <input type=text name=\"message_name\" required=true><br>";
	echo "<label>Message Body</label><br> <textarea id=\"message_body\" name=\"message_body\" rows=\"4\" cols=\"50\" required=true></textarea><br>";
	echo "<input type=submit name=\"message_submit\" value=\"Send Message\"> <input type=reset name=\"message_reset\">";
	echo "</form>";

	if(isset($_POST["message_submit"])){ do{

		$contacts = $database->query("select Users.display_name, Users.user_id from User_Contacts, Users where contacter_id=$user[user_id] and contact_id=user_id");
		echo "$database->error";
		$num_contacts = $contacts->num_rows;
		$people_messaged = 0;

		for($i = 0; $i < $num_contacts; $i++){
			if(isset($_POST["contact$i"])){
				$receiver_id = $_POST["contact$i"];
				$database->query("insert into Messages (sender_id, receiver_id, title, message, send_date) values ($user[user_id], $receiver_id, \"$_POST[message_name]\", \"$_POST[message_body]\", now())");
				echo "$database->error";
				$people_messaged++;
			}
		}

		if($people_messaged == 1){
			echo "Message submitted to $people_messaged person";
		}
		else if($people_messaged){
			echo "Message submitted to $people_messaged people";
		}
		else{
			echo "Nice message and all, but you didn't select anybody to send it to!";
		}

	} while(false); }
?>

<?php
	$database->close();
?>

</body>

</html>
