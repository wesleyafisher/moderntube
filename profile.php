<!doctype html>

<html>

<head>
<title> Profile - ModernTube </title>
<link rel="stylesheet" type="text/css" href="moderntube.css">
</head>

<body>

<a href="index.php">Home</a>

<?php
	session_start();
	include_once("database.php");

	$user = $database->query("select * from Users where username=\"$_SESSION[login_user]\"")->fetch_assoc();
	echo "$database->error";

	echo "<h1>Welcome to your profile $user[display_name]</h1>";
?>

<p> User Stats </p>

<?php

	echo "<label>Join date: $user[join_date]</label><br>";
	
	$num_uploads = $database->query("select count(media_id) as num from Media where uploader_id=\"$user[user_id]\"")->fetch_assoc()["num"];
	echo "$database->error";
	echo "<label>Number of uploads: $num_uploads</label><br>";

	$total_views = $database->query("select sum(views) as num from Media where uploader_id=\"$user[user_id]\"")->fetch_assoc()["num"];
	echo "$database->error";
	if(!$total_views) $total_views = 0;
	echo "<label>Total views: $total_views</label><br>";

	echo "<a href=\"channel.php?user_id=$user[user_id]\"> My Channel </a><br>";
	echo "<a href=\"messagecenter.php\"> Message Center </a>";

?><br>

<p> Playlists </p>

<?php

	$playlists = $database->query("select playlist_id, name from Playlists where playlist_creator_id=\"$user[user_id]\"");
	echo "$database->error";
	while($row = $playlists->fetch_assoc()){
		echo "<a href=\"playlist.php?playlist_id=$row[playlist_id]\">$row[name]</a><br>";
	}
	echo "<br>";

?>

<form action="profile.php" method="post">
	<label>Add Playlist</label><br>
	<label>Playlist Name:</label><input type="text" name="playlist_name" required="true"><br>
	<input type="submit" name="add_playlist_submit" value="Add Playlist"><br>
</form>

<?php

	if(isset($_POST["add_playlist_submit"])){ do{

		// Add playlist
		$database->query("insert into Playlists (playlist_creator_id, name, creation_date) values (\"$user[user_id]\", \"$_POST[playlist_name]\", now())");
		echo "$database->error";
		echo "Playlist added";

	} while(false); }

?><br>

<form action="profile.php" method="post">
	<label>Reset Password</label><br>
	<label>Current Password:</label><input type="password" name="oldpassword" required="true"><br>
	<label>New Password:</label><input type="password" name="newpassword1" required="true"><br>
	<label>Confirm New Password:</label><input type="password" name="newpassword2" required="true"><br>
	<input type="submit" name="reset_password_submit" value="Reset Password"><br>
</form>

<?php

	if(isset($_POST["reset_password_submit"])){ do{

		// Check if user entered current password correctly
		if($_POST["oldpassword"] != $user["password"]) {
			echo "Your current password is wrong, try again.";
			break;
		}

		// Check if passwords match
		if($_POST["newpassword1"] != $_POST["newpassword2"]) {
			echo "Your passwords don't match, try again.";
			break;
		}

		// Change their password
		$database->query("update Users set password=\"$_POST[newpassword1]\" where user_id=\"$user[user_id]\"");
		echo "$database->error";
		echo "Password updated";

	} while(false); }

?>

<br>

<form action="profile.php" method="post">
	<label>Change Display Name</label><br>
	<label>New Display Name:</label><input type="text" name="new_display_name" required="true"><br>
	<input type="submit" name="change_display_name_submit" value="Change Display Name"><br>
</form>

<?php

	if(isset($_POST["change_display_name_submit"])){ do{

		// Change their display name
		$database->query("update Users set display_name=\"$_POST[new_display_name]\" where user_id=\"$user[user_id]\"");
		echo "$database->error";
		echo "Display name updated";

	} while(false); }

?>

<?php
	$database->close();
?>

</body>

</html>
